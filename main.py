import json


# map the mercarto category titles to the ali express categories and get the ids
# we want these ids so we can then search by their products.
def categoryIdMapper():
    with open('data/mercategories.json') as json_file:
        mercarto_cats = json.load(json_file)['categories']

        with open('mocks/mockCategoriesResponse.json') as f:
            ali_categories = json.load(f)['categories']

        cat_id_mapping = [{'mercarto_id': m['id'], 'ali_id': a['id'], 'name': m['name']}
                          for m in mercarto_cats for a in ali_categories if a['name'] == m['name']]
        print(cat_id_mapping)

        with open('data/catIdMapping.json', 'w', encoding='utf-8') as f:
            json.dump(cat_id_mapping, f, ensure_ascii=False, indent=4)


if __name__ == "__main__":
    categoryIdMapper()
