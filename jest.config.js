module.exports = {
  preset: "./jest-preset.js",
  testEnvironment: "node",
  globals: {
    "ts-jest": {
      tsConfig: {
        sourceMap: false
      }
    }
  }
};
