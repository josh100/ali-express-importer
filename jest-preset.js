module.exports = {
  coverageDirectory: "./coverage",
  moduleFileExtensions: ["js", "json", "ts", "tsx"],
  roots: ["src"],
  testMatch: ["<rootDir>/**/__tests__/*.(spec|test).(ts|tsx)"],
  transform: {
    "^.+\\.tsx?$": "ts-jest"
  },
  reporters: ["default"],
  transformIgnorePatterns: [
    "packages/[^/]*/lib/.*",
    ".*(node_modules)(?!.*@mercarto.*).*$"
  ],
  testPathIgnorePatterns: ["/node_modules/", "/<rootDir>/lib/"]
};
