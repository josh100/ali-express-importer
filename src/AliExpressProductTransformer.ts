// Actual implementation from ali express product into a importable format for mercarto to consume

import * as crypto from "crypto";
import * as fs from "fs";
import { logger } from "./logger";
import {
  AliExpressProduct,
  ProductSKUPropertyList,
  SkuModule,
  SkuPriceList,
  SkuPropertyValue,
} from "./typings/AliExpressProduct";
import { CategoryMapping } from "./typings/CategoryMapping";
import {
  DimensionsCm,
  Image,
  ImportConfig,
  LocalisedProductProperties,
  MercartoImportableProduct,
  Option,
  Price,
  Variant,
  VariantOption,
} from "./typings/MercartoImportableProduct.d";

export function createSlug(str: string): string {
  // lowercase and hypenate
  const noSpaceString = str.trim().replace(/\s+/g, "-").toLowerCase(); // replace whitespace with dash
  return noSpaceString
    .replace(/[^a-zA-Z0-9\-]+/g, "")
    .replace(/(\-+-+)+/g, "-"); // replace consequetive dashes with single dash
}

export function createVariantsFromAeProduct(
  product: AliExpressProduct
): Variant[] {
  // sellable variants
  return product.skuModule.skuPriceList.map((skuPrice) => {
    const { options, images } = getOptionsAndImagesOnVariant(
      skuPrice.skuPropIds,
      product.skuModule.productSKUPropertyList
    );
    return {
      id: crypto
        .createHash("sha256")
        .update(`${product.commonModule.productId + skuPrice.skuId}`)
        .digest("hex"),
      sku: skuPrice.skuId.toString(),
      name: "", // what should this be? It doesnt make sense to have a name? as a variant is just a different combination of sku property values
      upc: skuPrice.skuId.toString(), // TODO: not sure how to map
      prices: mapPricesForSku(skuPrice), // TODO: this should be price for this variant
      attributes: [],
      dimensionsCm: getDimensionsCm(),
      weightKg: 0, // TODO
      options,
      images,
      locations: [
        {
          locationId: 1,
          stockCount: skuPrice.skuVal.availQuantity,
        },
      ],
    };
  });
}

// for now assumes that the shipping price and variants prices are same currency
export function applyShippingCostToVariants(
  shippingPrice: Price,
  variants: Variant[]
): Variant[] {
  return variants.map((variant) => {
    return {
      ...variant,
      prices: variant.prices.map((price) => {
        return {
          value: price.value + shippingPrice.value,
          currency: "GBP", //price.currency,
        };
      }),
    };
  });
}

export function getProductImages(product: AliExpressProduct): Image[] {
  return product.imageModule.imagePathList.map((url) => {
    return {
      url,
      alt: "", // not sure what to do for this or description
      description: "",
    };
  });
}

// gets all images from the variants
export function getImagesFromSKuModule(skuModule: SkuModule): Image[] {
  // create a list of sku values with the image property
  let skusWithImages: SkuPropertyValue[] = [];
  skuModule.productSKUPropertyList.map((propertyList) => {
    skusWithImages.push(
      ...propertyList.skuPropertyValues.filter(
        (val) => val.skuPropertyImagePath
      )
    );
  });

  return getSkuImagesFromProperties(skusWithImages);
}

function getSkuImagesFromProperties(skuProps: SkuPropertyValue[]): Image[] {
  return skuProps.map((skuVal) => {
    return getSkuImageFromProperty(skuVal);
  });
}

function getSkuImageFromProperty(skuProp: SkuPropertyValue): Image {
  return {
    alt: skuProp.skuPropertyValueTips,
    description: skuProp.propertyValueDefinitionName,
    url: skuProp.skuPropertyImagePath ? skuProp.skuPropertyImagePath : "",
  };
}

// TODO: get product variant dimensions
function getDimensionsCm(): DimensionsCm {
  return {
    x: 0,
    y: 0,
    z: 0,
  };
}

/**
 *
 * @param skuPropIds a string in the format id, id where the ids relate to the sku property value ids
 * @param productSKUPropertyList list which contains the variants we need to find the matching ids to
 */
export function getOptionsAndImagesOnVariant(
  skuPropIds: string,
  productSKUPropertyList: ProductSKUPropertyList[]
): { options: Option[]; images: Image[] } {
  const ids = skuPropIds.split(",");
  const options: Option[] = [];
  const images: Image[] = [];
  ids.map((id) => {
    productSKUPropertyList.map((prop) => {
      const value = prop.skuPropertyValues.find(
        (prop) => prop.propertyValueId.toString() === id
      );
      if (value) {
        if (value.skuPropertyImagePath) {
          images.push(getSkuImageFromProperty(value));
        }
        options.push({
          name: prop.skuPropertyName,
          value: value.propertyValueName,
        });
      }
    });
  });
  return { options, images };
}

export function getVariantOptions(product: AliExpressProduct): VariantOption[] {
  return product.skuModule.productSKUPropertyList.map((skuProperty) => {
    return {
      name: skuProperty.skuPropertyName,
      values: skuProperty.skuPropertyValues.map(
        (val) => val.propertyValueDisplayName
      ),
    };
  });
}

export function mapPricesForSku(skuPrice: SkuPriceList): Price[] {
  return [
    {
      currency: "GBP", //skuPrice.skuVal.skuAmount.currency,
      value: skuPrice.skuVal.skuAmount.value, // the importer converts to pence so no need to do here
    } as Price,
  ];
}

export function getMercartoCategoryId(aliCategoryId: string): string {
  // read from file and return the mapped value
  let jsonData: CategoryMapping[] = JSON.parse(
    fs.readFileSync("data/catIdMapping.json", "utf8")
  );

  const obj: CategoryMapping | undefined = jsonData.find(
    (obj: CategoryMapping) => obj.ali_id === aliCategoryId
  );
  console.log("obj: ", obj);
  if (!obj) {
    throw new Error(
      "Mercarto id not found for category with id: " + aliCategoryId
    );
  }
  return obj.mercarto_id;
}

function getI18nProductInfo(
  product: AliExpressProduct
): LocalisedProductProperties[] {
  return [
    {
      description: product.pageModule.description,
      extendedContent: [],
      language: product.webEnv.language,
      shortDescription: product.pageModule.title,
      slug: createSlug(product.pageModule.title),
      title: product.pageModule.title,
    },
  ];
}

/**
 *
 * @param product Ali express product
 * @returns mercarto json that we can then use to import products
 */
export function transformAeProductToMercarto(
  product: AliExpressProduct
): MercartoImportableProduct {
  // may need publishedToMarketplace???
  const mercartoProduct: MercartoImportableProduct = {
    id: product.commonModule.productId.toString(),
    slug: createSlug(product.pageModule.title), // can possibly be the product title?
    supplier: 1, // mercarto is the supplier
    active: true,
    title: product.pageModule.title,
    brand: "f120a261-5f19-a606-9358-f49b6f423c61", // refers to an id meaning we need to also do a brand importer? - or dan say just classify as unbranded,
    // for now ive hardcoded brand and will import a default Ali Express brand
    category: getMercartoCategoryId(product.commonModule.categoryId.toString()),
    shortDescription: product.pageModule.title,
    description: product.pageModule.description,
    extendedContent: [],
    images: getProductImages(product),
    variantAttributes: [],
    variantOptions: getVariantOptions(product),
    variants: createVariantsFromAeProduct(product),
    priceType: "RETAIL", // this is unused??
    attributes: [],
    i18n: getI18nProductInfo(product),
    addons: [],
    meta: {
      title: "",
      description: "",
    },
    language: product.webEnv.language,
  };
  return mercartoProduct;
}

export async function saveProductConfig(
  products: MercartoImportableProduct[]
): Promise<{ json: ImportConfig; file: string }> {
  const json: ImportConfig = {
    products: [],
    categories: [],
    brands: [
      {
        id: "f120a261-5f19-a606-9358-f49b6f423c61",
        name: "No Brand",
        description: "No Brand",
        i18n: [],
        language: "en",
        bannerImage: {
          alt: "AliExpress Banner",
          description: "AliExpress Banner",
          url: "https://picsum.photos/800/200",
        },
      },
    ],
  };
  json.products.push(...products);
  const jsonString: string = JSON.stringify(json);
  const fileName = "data/aliexpressproducts" + Date.now() + ".json";
  fs.writeFile(fileName, jsonString, (err) => {
    if (err) {
      logger.error("Error saving file: ", err);
      throw err;
    }
  });

  return { json, file: fileName };
}
