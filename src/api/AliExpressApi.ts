import axios from "axios";
import dotenv from "dotenv";
import * as fs from "fs";
import { AliExpressProduct } from "src/typings/AliExpressProduct";
import {
  applyShippingCostToVariants,
  saveProductConfig,
  transformAeProductToMercarto,
} from "../AliExpressProductTransformer";
import { logger } from "../logger";
import { CategoryMapping } from "../typings/CategoryMapping";
import {
  MercartoImportableProduct,
  Price,
} from "../typings/MercartoImportableProduct.d";
import { ProductsByCategoryResponse } from "../typings/ProductsByCategoryResponse.d";
import { ShippingResult } from "../typings/ShippingResult.d";

dotenv.config(); // loads env variables into process.env

/**
 * Goes through each category which we have a mapping for
 * find each product in each category
 * query the product and its shipping
 * cycle through the pagination on the productsByCategory (not done)
 *
 * only import products with shipping == ePacket
 *
 * set product price to be base price + shipping price (this allows us to set shipping to free)
 *
 * save the JSON config
 *
 * TODO: create another function which calls the mercarto importer?
 */

export const API_URL = "https://ali-express1.p.rapidapi.com";
const SHIPPING_CO = "ePacket";
const SHIPPING_PARAMS = {
  destination_country: "UK",
};

export const HEADERS = {
  "content-type": "application/octet-stream",
  "x-rapidapi-host": "ali-express1.p.rapidapi.com",
  "x-rapidapi-key": process.env.RAPID_API_KEY,
};

// calls api and gets shipping price for the requested company e.g. ePacket
export async function getShippingPrice(
  shippingCompany: string,
  url: string,
  productId: number
): Promise<Price> {
  // get shipping
  console.log(`hitting url: ${url}/shipping/${productId}`);

  const response = await axios({
    method: "GET",
    url: `${url}/shipping/${productId}`,
    headers: HEADERS,
    params: SHIPPING_PARAMS,
  });

  if (response.status === 200) {
    const shippingResult: ShippingResult = response.data;
    const shipping = shippingResult.body?.freightResult.find(
      (freightResult) => {
        freightResult.company === shippingCompany;
      }
    );

    if (shipping) {
      return {
        value: Math.trunc(shipping.freightAmount.value * 100),
        currency: "GBP", //shipping.freightAmount.currency
      };
    } else {
      throw new Error(
        "Shipping does not exist for company: " + shippingCompany
      );
    }
  } else {
    throw new Error(`Failed to get shipment for product: ${productId}`);
  }
}

// returns AliExpress product by its product id
export async function getProductById(
  url: string,
  productId: number
): Promise<AliExpressProduct> {
  const response = await axios.get<AliExpressProduct>(
    `${url}/product/${productId}`,
    {
      headers: HEADERS,
      params: {
        language: "en",
      },
    }
  );

  logger.info("product response: ", response);
  if (response.status !== 200) {
    throw new Error(`Cant get product with id: ${productId}`);
  }
  return response.data;
}

export async function main() {
  console.log("running ali express importer...");

  const categoryMappings: CategoryMapping[] = JSON.parse(
    fs.readFileSync("data/catIdMapping.json", "utf8")
  );

  if (!categoryMappings) {
    logger.error("Couldnt load category mappings, exiting now");
    process.exit(1);
  }

  // if program breaks we should know where to continue from
  let lastProductId: number;
  let lastCategoryId: string;

  categoryMappings.map(async (catMap) => {
    lastCategoryId = catMap.ali_id;
    const productsByCategory: ProductsByCategoryResponse = JSON.parse(
      fs.readFileSync("mocks/mockProductsByCategoriesResponse.json", "utf8")
    );
    if (productsByCategory && !productsByCategory.error) {
      // TODO: figure out the pagination for this query - not sure if api lets us use cursor yet
      const productIds = productsByCategory.body.data.items.map(
        (item) => item.productId
      );
      console.log("product ids: ", productIds);

      let transformedProducts: MercartoImportableProduct[] = [];

      productIds.map(async (id) => {
        const [product, price] = await Promise.all([
          getProductById(API_URL, id),
          getShippingPrice(SHIPPING_CO, API_URL, id),
        ]);
        console.log("product: ", product);
        console.log("price: ", price);
        product.commonModule.categoryId = parseInt(lastCategoryId, 10);

        const mercartoProduct = transformAeProductToMercarto(product);
        mercartoProduct.variants = applyShippingCostToVariants(
          price,
          mercartoProduct.variants
        );

        transformedProducts.push(mercartoProduct);
        lastProductId = id;
      });

      const { json: config } = await saveProductConfig(transformedProducts);
      // TODO: upload config to mercarto to import...
    }
  });

  console.log("program complete, exiting...");
}
