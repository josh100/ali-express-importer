import { API_URL, getProductById, getShippingPrice } from "../AliExpressApi";

describe("AliExpress api", () => {
  it("should fail to get invalid product", async () => {
    const failingAsync = async () =>
      getProductById(API_URL, 999999999999999999999999);
    await expect(failingAsync()).rejects.toThrow();
  });
  it("should get valid product", async () => {
    const id = 4000479928418;
    const response = await getProductById(API_URL, id);
    expect(response).toBeDefined();
    expect(response.commonModule.productId).toBe(id);
  });
  it("should fail to get shipping for invalid product", async () => {
    const failingAsync = async () =>
      getShippingPrice("ePacket", API_URL, 999999999999999999999999);
    await expect(failingAsync()).rejects.toThrow();
  });
  it("should fail to get price for a shipping company that does not exist", async () => {
    const failingAsync = async () =>
      getShippingPrice("not a company-blvbrelvber", API_URL, 4000479928418);

    await expect(failingAsync()).rejects.toThrowError(
      "Shipping does not exist for company: not a company-blvbrelvber"
    );
  });
});
