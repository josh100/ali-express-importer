export as namespace ShippingResult;

export = ShippingResult;

declare module ShippingResult {
  export interface Features {}

  export interface BizShowMind {
    layout: any[];
  }

  export interface Features2 {}

  export interface FreightAmount {
    currency: string;
    formatedAmount: string;
    value: number;
  }

  export interface I18nMap {}

  export interface PlaceHolderMap {
    freightDisplayName: string;
    fromCountry: string;
    toIcon: string;
    fromIcon: string;
    toCountry: string;
  }

  export interface FromToInfo {
    medusaText: string;
    placeHolderMap: PlaceHolderMap;
  }

  export interface DeliveryDisplayModel {
    deliveryTime: string;
    fromToInfo: FromToInfo;
  }

  export interface PlaceHolderMap2 {
    shippingFee: string;
  }

  export interface ShippingFee {
    medusaText: string;
    placeHolderMap: PlaceHolderMap2;
  }

  export interface ShippingFeeInfoModel {
    shippingFee: ShippingFee;
  }

  export interface LtDisplayModel {
    deliveryDisplayModel: DeliveryDisplayModel;
    highLight: any[];
    serviceInfo: any[];
    shippingFeeInfoModel: ShippingFeeInfoModel;
  }

  export interface StandardFreightAmount {
    currency: string;
    formatedAmount: string;
    value: number;
  }

  export interface FreightResult {
    bizShowMind: BizShowMind;
    commitDay: string;
    company: string;
    currency: string;
    cutTime: string;
    deliveryDate: string;
    deliveryDateCopy: string;
    deliveryDateDisplay: string;
    deliveryDateFormat: string;
    discount: number;
    displayType: string;
    features: Features2;
    freightAmount: FreightAmount;
    fullMailLine: boolean;
    hbaService: boolean;
    i18nMap: I18nMap;
    id: number;
    ltDisplayModel: LtDisplayModel;
    name: string;
    notification: string;
    remainTime: string;
    sendGoodsCountry: string;
    sendGoodsCountryFullName: string;
    serviceName: string;
    standardFreightAmount: StandardFreightAmount;
    time: string;
    tracking: boolean;
  }

  export interface I18nMap2 {}

  export interface Body {
    features: Features;
    freightResult: FreightResult[];
    i18nMap: I18nMap2;
    name: string;
  }

  export interface ShippingResult {
    body?: Body;
    code: number;
    cost: number;
    success: boolean;
  }
}
