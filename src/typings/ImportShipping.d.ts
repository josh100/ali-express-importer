export as namespace ImportShipping;

export = ImportShipping;

declare module ImportShipping {
  export interface ImportShippingConfig {
    options: Partial<ShippingOption>[];
  }

  export interface ShippingOption {
    id?: number;
    courier: string;
    description: string;
    minDeliveryTime: string;
    maxDeliveryTime: string;
    rules?: ShippingOptionRule[];
  }

  export interface ShippingOptionRule {
    id?: number;
    countryCondition?: CountryCondition;
    weightCondition?: WeightCondition;
    orderTotalCondition?: OrderTotalCondition;
    isFree: boolean;
    prices: ShippingOptionPrice[];
  }

  export interface CountryCondition {
    allowedCountries: string[];
  }

  export interface WeightCondition {
    minWeightKg: number;
    maxWeightKg: number;
  }

  export interface OrderTotalCondition {
    minimumOrderTotal: number;
  }

  export interface ShippingOptionPrice {
    value: number;
    currency: string;
  }
}
