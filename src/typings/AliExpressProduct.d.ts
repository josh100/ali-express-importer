// generated with json to TS tool: http://www.json2ts.com/

export as namespace AliExpressProductModule;

export = AliExpressProductModule;

declare namespace AliExpressProductModule {
  export interface AliExpressProduct {
    actionModule: ActionModule;
    buyerProtectionModule: BuyerProtectionModule;
    commonModule: CommonModule;
    couponModule: CouponModule;
    crossLinkModule: CrossLinkModule;
    descriptionModule: DescriptionModule;
    features: Features7;
    feedbackModule: FeedbackModule;
    groupShareModule: GroupShareModule;
    i18nMap: I18nMap9;
    imageModule: ImageModule;
    installmentModule: InstallmentModule;
    middleBannerModule: MiddleBannerModule;
    name: string;
    otherServiceModule: OtherServiceModule;
    pageModule: PageModule;
    preSaleModule: PreSaleModule;
    priceModule: PriceModule;
    quantityModule: QuantityModule;
    recommendModule: RecommendModule;
    redirectModule: RedirectModule;
    shippingModule: ShippingModule;
    skuModule: SkuModule;
    specsModule: SpecsModule;
    storeModule: StoreModule;
    titleModule: TitleModule;
    webEnv: WebEnv;
  }

  export interface Features {}

  export interface I18nMap {
    WISH_MAX_NOTICE: string;
    BUYER_ABLE: string;
    WISHLIST_SAVE_BUTTON: string;
    WISH_MOVE_TO_ANOTHER_LIST_TIPS: string;
    ADD_X_MORE: string;
    SC_ADD_SUCC: string;
    WISHLIST_PUBLIC_NOTICE: string;
    WISH_DETAULT_LIST: string;
    WISH_CREATE_LIST: string;
    WL_ERROR: string;
    WISH_NAME_ALREADY_USE: string;
    WISH_REVISELIST: string;
    SC_ADD_FAILED: string;
    SC_HAVE: string;
    ADD_TO_CART: string;
    WISH_CANCEL_WISHLIST: string;
    BUY_NOW: string;
    WISH_SYSTEM_ERROR: string;
    SC_ADD_MAX: string;
    SC_VIEW: string;
    WISH_YOUMAYLIKE: string;
    WISH_VIEW_WISH_LIST: string;
    SC_RECOMMEND: string;
    CONTINUE: string;
    ADDED_TO: string;
    ORDER_NOW: string;
    SELECT_TIP: string;
    NO_LONGER_AVAILABLE: string;
    PREVIEW_PERIOD: string;
    WISH_MAX_GROUP: string;
    WISHLIST_PUBLIC_TIP: string;
  }

  export interface ActionModule {
    addToCartUrl: string;
    aeOrderFrom: string;
    allowVisitorAddCart: boolean;
    cartDetailUrl: string;
    categoryId: number;
    companyId: number;
    confirmOrderUrl: string;
    features: Features;
    i18nMap: I18nMap;
    id: number;
    itemStatus: number;
    itemWished: boolean;
    itemWishedCount: number;
    localSeller: boolean;
    name: string;
    productId: number;
    showCoinAnim: boolean;
    storeNum: number;
    totalAvailQuantity: number;
  }

  export interface BuyerProtection {
    brief: string;
    detailDescription: string;
    id: number;
    name: string;
    redirectUrl: string;
    type: number;
  }

  export interface Features2 {}

  export interface I18nMap2 {
    PLAZA_BUYER_PROTECTION_TITLE: string;
    PLAZA_BUYER_PROTECTION_TIP: string;
    PLAZA_BUYER_PROTECTION_CONTENT: string;
  }

  export interface BuyerProtectionModule {
    buyerProtection: BuyerProtection;
    features: Features2;
    i18nMap: I18nMap2;
    id: number;
    name: string;
  }

  export interface Features3 {}

  export interface I18nMap3 {}

  export interface ProductTags {}

  export interface CommonModule {
    categoryId: number;
    currencyCode: string;
    features: Features3;
    gagaDataSite: string;
    i18nMap: I18nMap3;
    id: number;
    name: string;
    plaza: boolean;
    productId: number;
    productTags: ProductTags;
    sellerAdminSeq: number;
    tradeCurrencyCode: string;
    userCountryCode: string;
    userCountryName: string;
  }

  export interface Features4 {}

  export interface I18nMap4 {
    GET_COUPONS: string;
    SCP_ERR_HAVE: string;
    OFF_ON: string;
    ORDER_OVER: string;
    code52Title: string;
    GET_IT: string;
    GET_NOW: string;
    GET_MORE: string;
    systemError: string;
    OFF_PER: string;
    STORE_COUPON: string;
    SHOP_COUPONE_TIME_START_END: string;
    NEW_USER_COUPON_ACQUIRE_FAIL_ALREADY_HAVE: string;
    code50Title: string;
    NEW_USER_COUPON_ACQUIRE_FAIL_NOT_AVAILABLE_NOW: string;
    NEW_USER_COUPON_ACQUIRE_FAIL_GROUP_LIMIT: string;
    code14Title: string;
    NEW_USER_COUPON_ACQUIRE_FAIL_NOT_NEW_USER: string;
    SHOP_PROMO_CODE_COPIED: string;
    ADDED: string;
    NEW_USER_COUPON_ACQUIRE_FAIL_SECURITY: string;
    SHOP_PROMO_CODE_TITLE: string;
    NEW_USER_COUPON_ACQUIRE_FAIL: string;
    NEW_USER_COUPON_ACQUIRE_FAIL_LIMIT_DAY: string;
    NEW_USER_COUPON_ACQUIRE_FAIL_REGISTER_COUNTRY_LIMIT: string;
    SCP_ERR_NONE: string;
    NEW_USER_COUPON_ACQUIRE_FAIL_GRANT_ERROR: string;
    code17Title: string;
    SHOP_COUPONE_TIME_EXPIRES: string;
    SHOPPONG_CREDIT: string;
    EXCHANGE_MORE: string;
    NEW_USER_COUPON_ACQUIRE_FAIL_SYSTEM_ERROR: string;
    MY_BALANCE: string;
    INSTANT_DISCOUNT: string;
    EXCHANGE_NOW: string;
    CROSS_STORE_VOUCHER_TIP: string;
    NEW_USER_COUPON: string;
    GET: string;
    SHOP_PROMO_CODE_COP_FAILED: string;
    MY_COINS: string;
    BUY_GET_OFF: string;
    code51Title: string;
    NEW_USER_COUPON_ACQUIRE_FAIL_LIMIT_HOUR: string;
    CROSS_STORE_VOUCHER: string;
  }

  export interface CouponModule {
    currencyCode: string;
    features: Features4;
    i18nMap: I18nMap4;
    id: number;
    name: string;
  }

  export interface BreadCrumbPathList {
    cateId: number;
    name: string;
    remark: string;
    url: string;
    target: string;
  }

  export interface CrossLinkList {
    displayName: string;
    name: string;
    url: string;
  }

  export interface CrossLinkGroupList {
    channel: string;
    crossLinkList: CrossLinkList[];
    name: string;
  }

  export interface Features5 {}

  export interface I18nMap5 {
    BREADCRUMB_PART2: string;
    BREADCRUMB_PART1: string;
    CROSSLINK_RELATED_SEARCHES: string;
  }

  export interface CrossLinkModule {
    breadCrumbPathList: BreadCrumbPathList[];
    crossLinkGroupList: CrossLinkGroupList[];
    features: Features5;
    i18nMap: I18nMap5;
    id: number;
    name: string;
    productId: number;
  }

  export interface Features6 {}

  export interface I18nMap6 {}

  export interface DescriptionModule {
    descriptionUrl: string;
    features: Features6;
    i18nMap: I18nMap6;
    id: number;
    name: string;
    productId: number;
    sellerAdminSeq: number;
  }

  export interface Features7 {}

  export interface Features8 {}

  export interface I18nMap7 {}

  export interface FeedbackModule {
    companyId: number;
    features: Features8;
    feedbackServer: string;
    i18nMap: I18nMap7;
    id: number;
    name: string;
    productId: number;
    sellerAdminSeq: number;
    trialReviewNum: number;
  }

  export interface Features9 {}

  export interface I18nMap8 {}

  export interface GroupShareModule {
    features: Features9;
    i18nMap: I18nMap8;
    id: number;
    name: string;
  }

  export interface I18nMap9 {
    VIEW_MORE: string;
    ASK_BUYERS: string;
    PAGE_NOT_FOUND_NOTICE: string;
    VIEW_5_MORE_ANSWERS: string;
    openTrace: string;
    PAGE_NOT_FOUND_RCMD_TITLE: string;
  }

  export interface Features10 {}

  export interface I18nMap10 {}

  export interface ImageModule {
    features: Features10;
    i18nMap: I18nMap10;
    id: number;
    imagePathList: string[];
    name: string;
    summImagePathList: string[];
  }

  export interface Features11 {}

  export interface I18nMap11 {}

  export interface InstallmentModule {
    features: Features11;
    i18nMap: I18nMap11;
    id: number;
    name: string;
  }

  export interface Features12 {}

  export interface I18nMap12 {
    END_IN: string;
    DAYS: string;
    DAY: string;
    STARTS_IN: string;
  }

  export interface MiddleBannerModule {
    features: Features12;
    i18nMap: I18nMap12;
    id: number;
    name: string;
  }

  export interface Features13 {}

  export interface I18nMap13 {
    TAB_SPECS: string;
    PLAZA_SERVICE_SUBTITLE_PC: string;
    PLAZA_SERVICE_WARRANTY_EMAIL: string;
    PLAZA_SERVICE_WARRANTY_PHONE: string;
    PLAZA_SERVICE_WARRANTY_HOURS: string;
    TAB_CUSTOMER_REVIEWS: string;
    PLAZA_SERVICE_WARRANTY_WEBSITE: string;
    TAB_OVERVIEW: string;
    PLAZA_SERVICE_WARRANTY_BRAND: string;
    PLAZA_SERVICE_WARRANTY_CATEGORY: string;
    PLAZA_SERVICE_TITLE_PC: string;
    PLAZA_SERVICE_CONTENT3_3_PC: string;
    PLAZA_SERVICE_WARRANTY_TITLE: string;
    TAB_REPORT_ITEM: string;
    TAB_SERVICE: string;
    PLAZA_SERVICE_CONTENT3_1_PC: string;
    PLAZA_SERVICE_CONTENT3_2_PC: string;
    PLAZA_SERVICE_CONTENT1_PC: string;
    PLAZA_SERVICE_SUBTITLE2_PC: string;
    PLAZA_SERVICE_CONTENT2_PC: string;
    PLAZA_SERVICE_SUBTITLE3_PC: string;
  }

  export interface OtherServiceModule {
    features: Features13;
    hasWarrantyInfo: boolean;
    i18nMap: I18nMap13;
    id: number;
    name: string;
  }

  export interface Features14 {}

  export interface I18nMap14 {}

  export interface MultiLanguageUrlList {
    language: string;
    languageUrl: string;
  }

  export interface PageModule {
    aeOrderFrom: string;
    aerSelfOperation: boolean;
    boutiqueSeller: boolean;
    complaintUrl: string;
    description: string;
    features: Features14;
    i18nMap: I18nMap14;
    id: number;
    imagePath: string;
    itemDetailUrl: string;
    keywords: string;
    kidBaby: boolean;
    mSiteUrl: string;
    multiLanguageUrlList: MultiLanguageUrlList[];
    name: string;
    ogDescription: string;
    ogTitle: string;
    ogurl: string;
    oldItemDetailUrl: string;
    plazaElectronicSeller: boolean;
    productId: number;
    ruSelfOperation: boolean;
    showPlazaHeader: boolean;
    siteType: string;
    spanishPlaza: boolean;
    title: string;
  }

  export interface Features15 {}

  export interface I18nMap15 {}

  export interface PreSaleModule {
    features: Features15;
    i18nMap: I18nMap15;
    id: number;
    name: string;
  }

  export interface Features16 {}

  export interface I18nMap16 {
    LOT: string;
    INSTALLMENT: string;
    DEPOSIT: string;
    PRE_ORDER_PRICE: string;
  }

  export interface MaxAmount {
    currency: string;
    formatedAmount: string;
    value: number;
  }

  export interface MinAmount {
    currency: string;
    formatedAmount: string;
    value: number;
  }

  export interface PriceModule {
    bigPreview: boolean;
    bigSellProduct: boolean;
    features: Features16;
    formatedPrice: string;
    hiddenBigSalePrice: boolean;
    i18nMap: I18nMap16;
    id: number;
    installment: boolean;
    lot: boolean;
    maxAmount: MaxAmount;
    minAmount: MinAmount;
    multiUnitName: string;
    name: string;
    numberPerLot: number;
    oddUnitName: string;
  }

  export interface Features17 {}

  export interface I18nMap17 {
    LOT: string;
    LOTS: string;
    BUY_LIMIT: string;
    QUANTITY: string;
    OFF_OR_MORE: string;
    ONLY_QUANTITY_LEFT: string;
    ADDTIONAL: string;
    QUANTITY_AVAILABLE: string;
  }

  export interface QuantityModule {
    displayBulkInfo: boolean;
    features: Features17;
    i18nMap: I18nMap17;
    id: number;
    lot: boolean;
    multiUnitName: string;
    name: string;
    oddUnitName: string;
    totalAvailQuantity: number;
  }

  export interface Features18 {
    recommendGpsScenarioOtherSellerProducts: string;
    showSubTitle: string;
    recommendGpsScenarioTopSelling: string;
    recommendGpsScenarioSellerOtherProducts: string;
  }

  export interface I18nMap18 {
    MORE_FROM_THIS_SELLER: string;
    YOU_MAY_LIKE: string;
    TOP_SELLING: string;
    FROM_OTHER_SELLER: string;
    VIEW_MORE_LINK: string;
    PRODUCT_SOLD: string;
  }

  export interface RecommendModule {
    categoryId: number;
    companyId: number;
    features: Features18;
    i18nMap: I18nMap18;
    id: number;
    name: string;
    productId: number;
    storeNum: number;
  }

  export interface Features19 {}

  export interface I18nMap19 {}

  export interface RedirectModule {
    bigBossBan: boolean;
    code: string;
    features: Features19;
    i18nMap: I18nMap19;
    id: number;
    name: string;
    redirectUrl: string;
  }

  export interface Features20 {}

  export interface I18nMap20 {
    FAST_SHIPPING: string;
    CAN_NOT_DELIVER: string;
    HBA_TRAKING_AVAILABLE: string;
    HAB_BALLOON_TRAKING_AVAILABLE: string;
    SELECT_SHIP_FROM_TIP: string;
    DAYS: string;
    SHIPPING_TO: string;
    HAB_SHIPPING_TO: string;
    CARRIER: string;
    FREE_SHIPPING: string;
    COST: string;
    BALLOON_TIP: string;
    SHIP_MY_ITEM_TO: string;
    HAB_BALLOON_VAT_INCLUDED: string;
    TRACKING: string;
    TO_COUNTRY: string;
    CAN_NOE_DELIVER_NOTE: string;
    ESTIMATED_DELIVERT_ON_DAYS: string;
    CHOOSE_DELIVERT_METHOD: string;
    HAB_BALLOON_DOOR_DELIVERY: string;
    DELIVERED_BY: string;
    HBA_SHIPPING_INFO: string;
    PLAZA_BALLOON_TIP: string;
    IN: string;
    SEARCH: string;
    SELECT_SHIP_FROM: string;
    HBA_TVAT_INCLUDED: string;
    ESTIMATED_DELIVERY: string;
    HBA_BALLOON_TIPS: string;
    VAT_DE_DETAIL: string;
    HBA_DOR_DELIVERY: string;
    VAT_NUMBER: string;
    ESTIMATED_DELIVERT_ON_DATE: string;
    OR_FULL_REFOUND: string;
    TO_VIA: string;
    APPLY: string;
    PLAZA_SHOP_NOW_RECEIVE_ON: string;
  }

  export interface ShippingModule {
    currencyCode: string;
    features: Features20;
    hbaFreight: boolean;
    i18nMap: I18nMap20;
    id: number;
    name: string;
    productId: number;
    regionCountryName: string;
    userCountryCode: string;
    userCountryName: string;
  }

  export interface Features21 {}

  export interface I18nMap21 {
    SIZING_INFO: string;
    BUST_PROMPT: string;
    GLASSES_DIALOG_TITLE: string;
    NV_ADD: string;
    SPH: string;
    PUPILLARY_PROMPT: string;
    SIZE_HOVER_TITLE: string;
    FLOOR_CONTENT: string;
    CUSTOM_SIZE_CONTENT: string;
    NV_ADD_PROMPT: string;
    PLEASE_ENTER: string;
    WAIST_TITLE: string;
    WAIST_CONTENT: string;
    CYL: string;
    SERVICE: string;
    BUST_CONTENT: string;
    SIZE_INFO: string;
    SIZE_INFO_DESC: string;
    ITEM_CONDITION_TIP: string;
    BTN_CANCEL: string;
    HOW_TO_MEASURE: string;
    SIZE_INFO_TIP: string;
    FLOOR_PROMPT: string;
    FLOOR_TITLE: string;
    SIZE_INFO_COMPARE_TIP: string;
    UNIT_PROMPT: string;
    SELECT: string;
    HIPS_TITLE: string;
    HEIGHT_PROMPT: string;
    WAIST_PROMPT: string;
    BTN_SAVE: string;
    TITLE_OPTIONAL: string;
    SIZE_DIALOG_TITLE: string;
    GLASSES_TIP: string;
    SIZE_CHART: string;
    HIPS_PROMPT: string;
    SPH_PROMPT: string;
    HIPS_CONTENT: string;
    BUST_TITLE: string;
    AXIS: string;
  }

  export interface SkuPropertyValue {
    propertyValueDefinitionName: string;
    propertyValueDisplayName: string;
    propertyValueId: number;
    propertyValueIdLong: number;
    propertyValueName: string;
    skuColorValue: string;
    skuPropertyImagePath?: string;
    skuPropertyImageSummPath?: string;
    skuPropertyTips: string;
    skuPropertyValueShowOrder: number;
    skuPropertyValueTips: string;
  }

  export interface ProductSKUPropertyList {
    isShowTypeColor: boolean;
    order: number;
    showType: string;
    showTypeColor: boolean;
    skuPropertyId: number;
    skuPropertyName: string;
    skuPropertyValues: SkuPropertyValue[];
  }

  export interface SkuAmount {
    currency: string;
    formatedAmount: string;
    value: number;
  }

  export interface SkuVal {
    availQuantity: number;
    inventory: number;
    isActivity: boolean;
    optionalWarrantyPrice: any[];
    skuAmount: SkuAmount;
    skuCalPrice: string;
    skuMultiCurrencyCalPrice: string;
    skuMultiCurrencyDisplayPrice: string;
  }

  export interface SkuPriceList {
    skuAttr: string;
    skuId: any;
    skuPropIds: string;
    skuVal: SkuVal;
  }

  export interface SkuModule {
    categoryId: number;
    features: Features21;
    forcePromiseWarrantyJson: string;
    hasSizeInfo: boolean;
    hasSkuProperty: boolean;
    i18nMap: I18nMap21;
    id: number;
    name: string;
    productSKUPropertyList: ProductSKUPropertyList[];
    skuPriceList: SkuPriceList[];
    warrantyDetailJson: string;
  }

  export interface Features22 {}

  export interface I18nMap22 {}

  export interface Prop {
    attrName: string;
    attrNameId: number;
    attrValue: string;
    attrValueId: string;
  }

  export interface SpecsModule {
    features: Features22;
    i18nMap: I18nMap22;
    id: number;
    name: string;
    props: Prop[];
  }

  export interface Features23 {}

  export interface I18nMap23 {
    COUSTOMER_SERVICE: string;
    VISIT_STORE: string;
    CONTACT_SELLER: string;
    FOLLOWING_STATE: string;
    UNFOLLOWING_STATE: string;
    POSITIVE_FEEDBACK: string;
    FOLLOWERS: string;
    FOLLOWER: string;
    TOP_SELLER: string;
    STORE_CATEGORIES: string;
  }

  export interface StoreModule {
    companyId: number;
    countryCompleteName: string;
    detailPageUrl: string;
    esRetailOrConsignment: boolean;
    features: Features23;
    feedbackMessageServer: string;
    feedbackServer: string;
    followed: boolean;
    followingNumber: number;
    hasStore: boolean;
    hasStoreHeader: boolean;
    hideCustomerService: boolean;
    i18nMap: I18nMap23;
    id: number;
    name: string;
    openTime: string;
    openedYear: number;
    positiveNum: number;
    positiveRate: string;
    productId: number;
    province: string;
    sellerAdminSeq: number;
    sessionId: string;
    siteType: string;
    storeName: string;
    storeNum: number;
    storeURL: string;
    topBrandDescURL: string;
    topRatedSeller: boolean;
  }

  export interface Features24 {}

  export interface FeedbackRating {
    averageStar: string;
    averageStarRage: string;
    display: boolean;
    evarageStar: string;
    evarageStarRage: string;
    fiveStarNum: number;
    fiveStarRate: string;
    fourStarNum: number;
    fourStarRate: string;
    oneStarNum: number;
    oneStarRate: string;
    positiveRate: string;
    threeStarNum: number;
    threeStarRate: string;
    totalValidNum: number;
    trialReviewNum: number;
    twoStarNum: number;
    twoStarRate: string;
  }

  export interface I18nMap24 {
    REVIEWS: string;
    VIEW_ALL_REVIEWS: string;
    REVIEW: string;
    VIEW_OTHER_TITLE: string;
    VIEW_EN_TITLE: string;
    FREEBIE_REVIEW: string;
    FREEBIE_REVIEWS: string;
  }

  export interface TitleModule {
    features: Features24;
    feedbackRating: FeedbackRating;
    formatTradeCount: string;
    i18nMap: I18nMap24;
    id: number;
    name: string;
    orig: boolean;
    origTitle: boolean;
    subject: string;
    tradeCount: number;
    tradeCountUnit: string;
    trans: boolean;
    transTitle: boolean;
  }

  export interface Env {
    valMap: string;
    zone: string;
  }

  export interface WebEnv {
    country: string;
    currency: string;
    env: Env;
    host: string;
    hostname: string;
    ip: string;
    lang: string;
    language: string;
    locale: string;
    reqHost: string;
    site: string;
  }
}
