export as namespace MercartoImportableProduct;

export = MercartoImportableProduct;

declare module MercartoImportableProduct {
  export interface LocalisedProductProperties {
    language: string;
    title: string;
    shortDescription: string;
    description: string;
    meta?: Meta;
    slug: string;
    extendedContent: ExtendedContent[];
  }

  export interface ExtendedContent {
    title: string;
    content: string;
  }

  export interface MercartoImportableProduct {
    supplier: number;
    active: boolean;
    language: string;
    slug: string;
    title: string;
    brand: string;
    category: string;
    shortDescription: string;
    description: string;
    extendedContent: ExtendedContent[];
    images: Image[];
    variantAttributes: VariantAttribute[];
    variantOptions: VariantOption[];
    priceType: string;
    addons: any[];
    attributes: Attribute[];
    i18n: LocalisedProductProperties[];
    meta: Meta;
    variants: Variant[];
    id: string;
  }
  export interface BannerImage {
    alt: string;
    description: string;
    url: string;
  }

  export interface Term {
    value: string;
    i18n: any[];
  }

  export interface Attribute {
    name: string;
    language: string;
    valueType: string;
    queryType: string;
    displayType: string;
    terms: Term[];
  }

  export interface Category {
    id: string;
    name: string;
    language: string;
    slug: string;
    parent: string;
    i18n: any[];
    bannerImage: BannerImage;
    attributes: Attribute[];
  }

  export interface Brand {
    id: string;
    name: string;
    language: string;
    description: string;
    bannerImage: BannerImage;
    i18n: any[];
  }

  export interface Image {
    alt: string;
    description: string;
    url: string;
  }

  export interface VariantAttribute {
    name: string;
    values: string[];
  }

  export interface VariantOption {
    name: string;
    values: string[];
  }

  export interface Meta {
    title: string;
    description: string;
  }

  export interface Attribute2 {
    name: string;
    value: string;
  }

  export interface Option {
    name: string;
    value: string;
  }

  export interface Price {
    value: number;
    currency: string;
  }

  export interface DimensionsCm {
    x: number;
    y: number;
    z: number;
  }

  export interface Variant {
    id: string;
    sku: string;
    upc: string;
    attributes: Attribute2[];
    options: Option[];
    prices: Price[];
    images: Image[];
    weightKg: number;
    dimensionsCm: DimensionsCm;
    name: string;
    locations?: SupplierProductLocation[];
  }

  export interface SupplierProductLocation {
    locationId: number;
    stockCount: number;
  }

  export interface Product {
    supplier: number;
    active: boolean;
    language: string;
    slug: string;
    title: string;
    brand: string;
    category: string;
    shortDescription: string;
    description: string;
    extendedContent: ExtendedContent[];
    images: Image[];
    variantAttributes: VariantAttribute[];
    variantOptions: VariantOption[];
    priceType: string;
    addons: any[];
    attributes: any[];
    i18n: any[];
    meta: Meta;
    variants: Variant[];
    id: string;
  }

  export interface ImportConfig {
    categories: Category[];
    brands: Brand[];
    products: Product[];
  }
}
