export as namespace ProductsByCategoryResponse;

export = ProductsByCategoryResponse;

declare module ProductsByCategoryResponse {
  export interface Image {
    elementId: number;
    elementType: string;
    imgHeight: number;
    imgUrl: string;
    imgWidth: number;
  }

  export interface Trade {
    elementId: number;
    elementType: string;
    tradeDesc: string;
  }

  export interface ViewMore {
    elementId: number;
    elementType: string;
  }

  export interface SellPrice {
    currency: string;
    formatedAmount: string;
    priceType: string;
    value: number;
  }

  export interface Price {
    sell_price: SellPrice;
  }

  export interface SellingPointTag {
    type: string;
    displayTagType: string;
    tagText: string;
  }

  export interface SellingPoint {
    order: any;
    sellingPointTag: SellingPointTag;
    sellingPointTagId: string;
    sellingPointType: string;
  }

  export interface Logistics {
    elementId: number;
    elementType: string;
    logisticsDesc: string;
  }

  export interface Title {
    elementId: number;
    elementType: string;
    title: string;
  }

  export interface Evaluation {
    starHeight: number;
    starRating: string;
    starUrl: string;
    starWidth: number;
  }

  export interface ProductElements {
    image: Image;
    trade: Trade;
    view_more: ViewMore;
    price: Price;
    selling_point: SellingPoint[];
    logistics: Logistics;
    title: Title;
    evaluation: Evaluation;
  }

  export interface Click {
    algo_pvid: string;
    haveSellingPoint: string;
    aem_p4p_click: string;
  }

  export interface Custom {}

  export interface DetailPage {
    algo_pvid: string;
    algo_exp_id: string;
    aem_p4p_detail: string;
  }

  export interface Exposure {
    sellingPoint: string;
    algo_exp_id: string;
    session_id: string;
    aem_p4p_exposure: string;
  }

  export interface Trace {
    click: Click;
    custom: Custom;
    detailPage: DetailPage;
    exposure: Exposure;
  }

  export interface AdTag {
    type: string;
    displayTagType: string;
    tagText: string;
  }

  export interface P4p {
    adTag: AdTag;
    clickUrl: string;
    sessionId: string;
  }

  export interface Item {
    action: string;
    itemDiffConfigId: number;
    productElements: ProductElements;
    productId: any;
    productType: string;
    status: number;
    trace: Trace;
    type: string;
    p4p: P4p;
  }

  export interface SupportFromArea {
    country: string;
  }

  export interface Logistics2 {
    supportFromAreas: SupportFromArea[];
  }

  export interface P4pInfo {
    p4pId: string;
  }

  export interface Param {
    affiliate: boolean;
    affiliateOnly: boolean;
    bigSale: boolean;
    categoryId: number;
    clientType: string;
    cna: string;
    cookieId: string;
    count: number;
    crawler: boolean;
    currency: string;
    deviceId: string;
    freeShipping: boolean;
    host: string;
    https: boolean;
    ignoreSPU: boolean;
    ip: string;
    jnTags: boolean;
    lang: string;
    language: string;
    mainSearch: boolean;
    mobileSale: boolean;
    newArriavl: boolean;
    onSale: boolean;
    onePiece: boolean;
    p4pPageId: string;
    pop: boolean;
    productType: string;
    scene: string;
    searchType: string;
    seo: boolean;
    shipToCountry: string;
    shoppingCoupon: boolean;
    site: string;
    start: number;
    ua: string;
  }

  export interface AttributeValue {
    count: number;
    id: number;
    inheritId: string;
    name: string;
    color: string;
    imagePath: string;
  }

  export interface Attribute2 {
    attributeValues: AttributeValue[];
    id: number;
    idList: string[];
    ids: string;
    name: string;
    nameList: string[];
    names: string;
    type: string;
    uniqueId: string;
  }

  export interface Attribute {
    attributes: Attribute2[];
  }

  export interface Base {
    hasNext: boolean;
    pageSize: number;
    totalNum: number;
  }

  export interface CategoryPath {
    id: number;
    isLeaf: boolean;
    name: string;
  }

  export interface ParentCategory {
    id: number;
    isLeaf: boolean;
    name: string;
  }

  export interface SelectedCategory {
    brotherCategories: any[];
    id: number;
    isLeaf: boolean;
    name: string;
    subCategories: any[];
  }

  export interface Category {
    categoryPath: CategoryPath[];
    parentCategory: ParentCategory;
    selectedCategory: SelectedCategory;
  }

  export interface Refine {
    attribute: Attribute;
    base: Base;
    category: Category;
  }

  export interface Page {
    ab: string;
    list_page_type: string;
    algo_pvid: string;
    aem_count: string;
    btsid: string;
    pid: string;
    tenant: string;
    aem_ab: string;
  }

  export interface Trace2 {
    bucketResult: string;
    magnetoIds: string;
    page: Page;
    physicalAbBucketResult: string;
  }

  export interface Data {
    categoryId: number;
    items: Item[];
    logistics: Logistics2;
    mainSearchURL: string;
    nextUrl: string;
    p4pInfo: P4pInfo;
    param: Param;
    refine: Refine;
    shoppingCouponActive: boolean;
    status: string;
    trace: Trace2;
  }

  export interface Body {
    code: number;
    cost: number;
    data: Data;
    success: boolean;
  }

  export interface ProductsByCategoryResponse {
    body: Body;
    headers: any;
    statusCode: number;
    error: boolean;
  }
}
