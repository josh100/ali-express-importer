export interface CategoryMapping {
  mercarto_id: string;
  ali_id: string;
  name: string;
}
