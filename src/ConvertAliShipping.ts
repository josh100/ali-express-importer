import {
  ImportShippingConfig,
  ShippingOption,
} from "./typings/ImportShipping.d";
import { ShippingResult } from "./typings/ShippingResult.d";

// map the required fields to the shipping config
export function convertAliShippingToMercartoConfig(
  shippingResult: ShippingResult
): ImportShippingConfig {
  if (shippingResult.code !== 200 || !shippingResult.success) {
    throw new Error("Result has errors");
  }
  if (!shippingResult.body || shippingResult.body.freightResult.length < 0) {
    throw new Error("Shipping result is empty");
  }
  if (shippingResult.cost < 0) {
    throw new Error("Shipping cost cant be negative");
  }
  return {
    options: shippingResult.body.freightResult.map((freightResult) => {
      const isFree: boolean =
        freightResult.ltDisplayModel.shippingFeeInfoModel.shippingFee
          .medusaText === "Free Shipping";

      const timeSegments = freightResult.time.split("-");
      return {
        courier: freightResult.company,
        description: freightResult.company,
        minDeliveryTime: convertShippingTimeToPeriod(timeSegments[0].trim()),
        maxDeliveryTime: convertShippingTimeToPeriod(timeSegments[1].trim()),
        rules: [
          {
            isFree,
            countryCondition: {
              allowedCountries: [
                freightResult.ltDisplayModel.deliveryDisplayModel.fromToInfo
                  .placeHolderMap.toCountry,
              ],
            },
            prices: !isFree
              ? [
                  {
                    currency: "GBP", //freightResult.freightAmount.currency,
                    value: Math.trunc(freightResult.freightAmount.value * 100), // convert to pence for mercarto db
                  },
                ]
              : [
                  {
                    value: 0,
                    currency: "GBP", //freightResult.freightAmount.currency
                  },
                ],
          },
        ],
      } as Partial<ShippingOption>;
    }),
  };
}

export function convertShippingTimeToPeriod(timeInDays: string): string {
  return "P" + timeInDays + "D";
}
