import * as fs from "fs";
import isEqual from "lodash/isEqual";
import {
  applyShippingCostToVariants,
  createSlug,
  getMercartoCategoryId,
  getOptionsAndImagesOnVariant,
  saveProductConfig,
  transformAeProductToMercarto,
} from "../AliExpressProductTransformer";
import { AliExpressProduct } from "../typings/AliExpressProduct.d";
import { Image, Option, Price } from "../typings/MercartoImportableProduct.d";

describe("Convert Ali express product to mercarto importable product", () => {
  // TODO: test the happy paths

  const aliProduct: AliExpressProduct = JSON.parse(
    fs.readFileSync("mocks/mockHoodieProduct.json", "utf8")
  );

  //   it("should successfully convert ali express product to mercarto importable product", () => {});

  //   it("should create all the product variants", () => {});

  //   it("should map variant options on a product", () => {});

  //   it("should get correct variant price", () => {});

  it("should get a variants options and images", () => {
    const expected: { options: Option[]; images: Image[] } = {
      options: [
        {
          name: "Color",
          value: "Blue",
        },
        {
          name: "Size",
          value: "40",
        },
      ],
      images: [
        {
          alt: "3",
          description: "3",
          url:
            "https://ae01.alicdn.com/kf/H8af619f9a90a40a7b1d6b12b0a9a320dr/woman-hoodies-runway-high-quality-long-sleeve-cotton-sweatshirts-female.jpg_640x640.jpg",
        },
      ],
    };
    const actual = getOptionsAndImagesOnVariant(
      aliProduct.skuModule.skuPriceList[0].skuPropIds,
      aliProduct.skuModule.productSKUPropertyList
    );
    expect(isEqual(actual, expected)).toBe(true);
  });

  it("should be able to get mercarto category id from map file", () => {
    const actual = getMercartoCategoryId("100003141");
    expect(actual).toBeDefined();
  });

  // TODO: test bad paths
  it("should throw when reading a category without a mapping", () => {
    expect(() => getMercartoCategoryId("0000000")).toThrowError(
      "Mercarto id not found for category with id: 0000000"
    );
  });

  // test slug is lowercase and hypennated
  it("should convert the slug to a suitable format", () => {
    const badSlug = "This slug should work";
    const expectedSlug = "this-slug-should-work";

    const actual = createSlug(badSlug);
    expect(actual).toBe(expectedSlug);
  });

  it("should remove non alphanumeric characters from slug", () => {
    const badSlug = "This slug //should work|z8{}Pi93";
    const expectedSlug = "this-slug-should-workz8pi93";

    const actual = createSlug(badSlug);
    expect(actual).toBe(expectedSlug);
  });

  it("should replace multiple dashes with one", () => {
    const badSlug = "$59 Woman hoodie from China | -AliExpress";
    const expectedSlug = "59-woman-hoodie-from-china-aliexpress";

    const actual = createSlug(badSlug);
    expect(actual).toBe(expectedSlug);
  });

  it("should apply the shipping price to the product variants", () => {
    const variants = [
      {
        id: "d1c8e5fe53d33e2b3a54dfe43fd43228750b5be7c6368b65571e52447501fdcc",
        sku: "10000004332368364",
        name: "",
        upc: "10000004332368364",
        prices: [{ currency: "GBP", value: 58 }],
        attributes: [],
        dimensionsCm: { x: 0, y: 0, z: 0 },
        weightKg: 0,
        options: [
          { name: "Color", value: "Blue" },
          { name: "Size", value: "40" },
        ],
        images: [
          {
            alt: "3",
            description: "3",
            url:
              "https://ae01.alicdn.com/kf/H8af619f9a90a40a7b1d6b12b0a9a320dr/woman-hoodies-runway-high-quality-long-sleeve-cotton-sweatshirts-female.jpg_640x640.jpg",
          },
        ],
        locations: [{ locationId: 1, stockCount: 30 }],
      },
    ];

    const shippingPrice: Price = {
      currency: "GBP",
      value: 12.5,
    };

    const expectedVariant = [
      {
        id: "d1c8e5fe53d33e2b3a54dfe43fd43228750b5be7c6368b65571e52447501fdcc",
        sku: "10000004332368364",
        name: "",
        upc: "10000004332368364",
        prices: [
          {
            currency: "GBP",
            value: variants[0].prices[0].value + shippingPrice.value,
          },
        ],
        attributes: [],
        dimensionsCm: { x: 0, y: 0, z: 0 },
        weightKg: 0,
        options: [
          { name: "Color", value: "Blue" },
          { name: "Size", value: "40" },
        ],
        images: [
          {
            alt: "3",
            description: "3",
            url:
              "https://ae01.alicdn.com/kf/H8af619f9a90a40a7b1d6b12b0a9a320dr/woman-hoodies-runway-high-quality-long-sleeve-cotton-sweatshirts-female.jpg_640x640.jpg",
          },
        ],
        locations: [{ locationId: 1, stockCount: 30 }],
      },
    ];

    expect(
      isEqual(
        applyShippingCostToVariants(shippingPrice, variants),
        expectedVariant
      )
    ).toBe(true);
  });

  it("should save the list of products in a format mercarto can import and the saved file should contain a time stamp", async () => {
    aliProduct.commonModule.categoryId = 100003141;
    const mercartoProduct = transformAeProductToMercarto(aliProduct);
    const { json, file } = await saveProductConfig([mercartoProduct]);

    expect(json).toBeDefined();

    const timestampFile = file.replace("data/aliexpressproducts", "");
    const timeStamp = timestampFile.substring(
      0,
      timestampFile.indexOf(".json")
    );

    expect(isNaN(parseInt(timeStamp, 10))).toBeFalsy();
  });
});
