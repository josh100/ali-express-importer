import * as fs from "fs";
import isEqual from "lodash/isEqual";
import { convertAliShippingToMercartoConfig } from "../ConvertAliShipping";
import { ImportShippingConfig } from "../typings/ImportShipping.d";
import { ShippingResult } from "../typings/ShippingResult.d";

describe("Convert shipping on product for mercarto", () => {
  const shippingMock: ShippingResult = JSON.parse(
    fs.readFileSync("mocks/mockShippingResponse.json", "utf8")
  );

  it("should fail if the response is not 200", () => {
    const failedShippping = Object.assign({}, shippingMock);
    failedShippping.code = 400;

    expect(() => convertAliShippingToMercartoConfig(failedShippping)).toThrow();
  });
  it("should fail if success is false", () => {
    const failedShippping = Object.assign({}, shippingMock);
    failedShippping.success = false;
    expect(() => convertAliShippingToMercartoConfig(failedShippping)).toThrow();
  });
  it("should fail if the body is empty", () => {
    const failedShippping = Object.assign({}, shippingMock);
    failedShippping.body = undefined;
    expect(() =>
      convertAliShippingToMercartoConfig(failedShippping)
    ).toThrowError("Shipping result is empty");
  });

  it("should fail if cost is negative", () => {
    const failedShippping = Object.assign({}, shippingMock);
    failedShippping.cost = -1;
    expect(() => convertAliShippingToMercartoConfig(failedShippping)).toThrow();
  });
  it("should convert the shipping into an importable format", () => {
    const expected: ImportShippingConfig = {
      options: [
        {
          courier: "AliExpress Standard Shipping",
          description: "AliExpress Standard Shipping",
          minDeliveryTime: "P20D",
          maxDeliveryTime: "P40D",
          rules: [
            {
              isFree: true,
              prices: [
                {
                  currency: "GBP",
                  value: 0,
                },
              ],
              countryCondition: {
                allowedCountries: ["United Kingdom"],
              },
            },
          ],
        },
        {
          courier: "DHL",
          description: "DHL",
          minDeliveryTime: "P7D",
          maxDeliveryTime: "P15D",
          rules: [
            {
              countryCondition: {
                allowedCountries: ["United Kingdom"],
              },
              isFree: false,
              prices: [
                {
                  currency: "GBP",
                  value: 3474,
                },
              ],
            },
          ],
        },
        {
          courier: "Fedex IP",
          description: "Fedex IP",
          minDeliveryTime: "P7D",
          maxDeliveryTime: "P15D",
          rules: [
            {
              isFree: false,
              prices: [
                {
                  currency: "GBP",
                  value: 7368,
                },
              ],
              countryCondition: {
                allowedCountries: ["United Kingdom"],
              },
            },
          ],
        },
      ],
    };

    const actual = convertAliShippingToMercartoConfig(shippingMock);

    expect(actual).toMatchObject<ImportShippingConfig>(expected);
    expect(isEqual(actual, expected)).toBeTruthy();
  });
});
